<?php
	
	require 'lib/function.php';
	
	pageheader("FAQ / Rules");
	
	$faq = "faq.html";
	
	if (!file_exists($faq))
	errorpage('There is no FAQ! Use common sense, I suppose, and you should be fine.');
	else echo file_get_contents($faq);
	pagefooter();
	
	?>
